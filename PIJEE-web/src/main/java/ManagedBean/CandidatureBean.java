package ManagedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import Services.CandidatureService;
import model.Candidature;


@ManagedBean(name = "candidatureBean")
@SessionScoped
public class CandidatureBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private int age;
	private String etat;
	private String first_Name;
	private String job;
	private String last_Name;
	private String sex;
	
	
	
	
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@EJB
	CandidatureService candidatureService;
	
	public void addCandidature() {
		candidatureService.addCandidature(new Candidature(age,etat,first_Name,job,last_Name,sex)); }

	

	
}
