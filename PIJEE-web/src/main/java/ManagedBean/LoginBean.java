package ManagedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import Services.CandidatService;
import model.Candidat;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@EJB
	CandidatService candidatService;
	
	private String password;
	private String email;
	private Candidat candidat;
	private boolean loggedIn;
	
	public CandidatService getCandidatService() {
		return candidatService;
	}
	public void setCandidatService(CandidatService candidatService) {
		this.candidatService = candidatService;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Candidat getCandidat() {
		return candidat;
	}
	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}
	
	
	public boolean isLoggedIn() {
		return loggedIn;
	}
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String doLogin() {
		String navigateTo = "null";
		candidat = candidatService.getEmployeByEmailAndPassword(email, password); 
	//if (employe != null && employe.getRole() == Role.administrateur) { 
		if (candidat != null && candidat.getType().equals("admin")) { 
		navigateTo = "/pages/Candidature/CandidatAff?faces-redirect=true"; 
		loggedIn = true; 
		}
		if (candidat != null && candidat.getType().equals("candidat")) { 
			navigateTo = "/pages/Candidature/CandidatAff?faces-redirect=true"; 
			loggedIn = true; }
		else { FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials")); 
		}
	return navigateTo; 
		}
	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession(); 
		return "/sign-in?faces-redirect=true"; 
	}
	
	
	
	


}
