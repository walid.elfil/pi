package ManagedBean;

import java.io.File;
import java.io.FileOutputStream;

import javax.servlet.http.Part;
import java.io.InputStream;
import java.io.Serializable;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


import Services.CandidatService;
import Services.CandidatureService;
import model.Candidat;
import model.Candidature;


@ManagedBean(name = "candidatBean")
@SessionScoped
public class CandidatBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
@EJB
CandidatService candidatService;	
	
	private String certification;
	private String competence;
	private String email;
	private String experience;
	private String introduction;
	private String nom;
	private String password;
	private String prenom;
	private String type;
	private Part img;
	private String c;
	private static int i;
	
	private List<Candidat> Candidats;
	
	
	public Part getImg() {
		return img;
	}
	public void setImg(Part img) {
		this.img = img;
	}
	private Integer CandidatIdToBeUpdated;
	public String getCertification() {
		return certification;
	}
	public void setCertification(String certification) {
		this.certification = certification;
	}
	public String getCompetence() {
		return competence;
	}
	public void setCompetence(String competence) {
		this.competence = competence;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	public CandidatService getCandidatService() {
		return candidatService;
	}
	public void setCandidatService(CandidatService candidatService) {
		this.candidatService = candidatService;
	}
	public List<Candidat> getCandidats() {
		return Candidats=candidatService.getAllCandidat();
	}
	public void setCandidats(List<Candidat> candidats) {
		Candidats = candidats;
	}




	
	public void addCandidat() {
		try{
			   
	           InputStream in=img.getInputStream();

	           File f=new File("C:/Work/eclipse-workspace/PIJEE/PIJEE-web/src/main/webapp/"+img.getSubmittedFileName());
	           f.createNewFile();
	           FileOutputStream out=new FileOutputStream(f);
	           
	           byte[] buffer=new byte[1024];
	           int length;
	           
	           while((length=in.read(buffer))>0){
	               out.write(buffer, 0, length);
	           }
	           
	           out.close();
	           in.close();
	           
	           FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("D:/document/4annee/s1/JEE/tps/Pidev/Pidev-web/src/main/webapp/assets/", f.getAbsolutePath());
	   		candidatService.addCandidat(new Candidat(certification,competence,email,experience="compte active",img.getSubmittedFileName(),nom,password,prenom,type));
 
	}catch(Exception e){
	           e.printStackTrace(System.out);
	       }
		final String username = "elfilwalid97@gmail.com";
        final String password = "wef09855238.";

        Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse("to_username_a@gmail.com, to_username_b@yahoo.com")
            );
            message.setSubject("Testing Gmail TLS");
            message.setText("Dear Mail Crawler,"
                    + "\n\n Please do not spam my email!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
	
	}
	public void removeCandidat(int candidatId)
	{
		candidatService.removeCandidatById(candidatId);
	}
	
	public void displayCandidat(Candidat candidat)
	{
		
		this.setCertification(candidat.getCertification());
		this.setCompetence(candidat.getCompetence());
		this.setEmail(candidat.getEmail());
		this.setExperience(candidat.getExperience());
		this.setIntroduction(candidat.getIntroduction());
		this.setNom(candidat.getNom());
		this.setPassword(candidat.getPassword());
		this.setPrenom(candidat.getPrenom());
		this.setType(candidat.getType());
		this.setCandidatIdToBeUpdated(candidat.getCandidatId());
		
		
	
		
		
	}
	public void updateCandidat()
	{ candidatService.updateCandidat(new Candidat(certification,competence,email,experience="compte banni",introduction,nom,password,prenom,type="compte suspendu",CandidatIdToBeUpdated)); 
	System.out.print("update candidat in bean");
	}
	
	
	public Integer getCandidatIdToBeUpdated() {
		return CandidatIdToBeUpdated;
	}
	public void setCandidatIdToBeUpdated(Integer candidatIdToBeUpdated) {
		CandidatIdToBeUpdated = candidatIdToBeUpdated;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int nbCompcand(String c)
	{
		System.out.println("hethi coprtenceeeeeeeeeeeeeee"+c);
		int i;
		i=candidatService.nbrCandidatparComp(c);
		return i;
		
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}

}
