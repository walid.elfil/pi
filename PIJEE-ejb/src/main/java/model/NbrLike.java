package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the NbrLikes database table.
 * 
 */
@Entity
@Table(name="NbrLikes")
@NamedQuery(name="NbrLike.findAll", query="SELECT n FROM NbrLike n")
public class NbrLike implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Candidat
	@ManyToOne
	@JoinColumn(name="CandidatId")
	private Candidat candidat;

	//bi-directional many-to-one association to Post
	@ManyToOne
	@JoinColumn(name="PostId")
	private Post post;

	public NbrLike() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Post getPost() {
		return this.post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}