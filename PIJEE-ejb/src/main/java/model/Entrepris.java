package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Entreprises database table.
 * 
 */
@Entity
@Table(name="Entreprises")
@NamedQuery(name="Entrepris.findAll", query="SELECT e FROM Entrepris e")
public class Entrepris implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EntrepriseId")
	private int entrepriseId;

	private String description;

	private int nbr_Employe;

	private int nbr_Point;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="entrepris")
	private List<Role> roles;

	public Entrepris() {
	}

	public int getEntrepriseId() {
		return this.entrepriseId;
	}

	public void setEntrepriseId(int entrepriseId) {
		this.entrepriseId = entrepriseId;
	}

	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNbr_Employe() {
		return this.nbr_Employe;
	}

	public void setNbr_Employe(int nbr_Employe) {
		this.nbr_Employe = nbr_Employe;
	}

	public int getNbr_Point() {
		return this.nbr_Point;
	}

	public void setNbr_Point(int nbr_Point) {
		this.nbr_Point = nbr_Point;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setEntrepris(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setEntrepris(null);

		return role;
	}

}