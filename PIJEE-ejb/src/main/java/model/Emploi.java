package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Emplois database table.
 * 
 */
@Entity
@Table(name="Emplois")
@NamedQuery(name="Emploi.findAll", query="SELECT e FROM Emploi e")
public class Emploi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Emploisid")
	private int emploisid;

	private String description;

	private String domaine;

	private String lieu;

	private String titre;

	public Emploi() {
	}

	public int getEmploisid() {
		return this.emploisid;
	}

	public void setEmploisid(int emploisid) {
		this.emploisid = emploisid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDomaine() {
		return domaine;
	}

	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	
}