package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the Comments database table.
 * 
 */
@Entity
@Table(name="Comments")
@NamedQuery(name="Comment.findAll", query="SELECT c FROM Comment c")
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Commentid")
	private int commentid;

	private String commentaire;

	@Column(name="DatedeCreation")
	private Date datedeCreation;

	//bi-directional many-to-one association to Post
	@ManyToOne
	@JoinColumn(name="postidfk")
	private Post post;

	public Comment() {
	}

	public int getCommentid() {
		return this.commentid;
	}

	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}

	

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Date getDatedeCreation() {
		return datedeCreation;
	}

	public void setDatedeCreation(Date datedeCreation) {
		this.datedeCreation = datedeCreation;
	}

	public Post getPost() {
		return this.post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

}