package model;

import java.io.Serializable;

import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Candidats database table.
 * 
 */
@Entity
@Table(name="Candidats")
@NamedQuery(name="Candidat.findAll", query="SELECT c FROM Candidat c")
public class Candidat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CandidatId")
	private int candidatId;
	
	@Column(name="Certification")
	private String certification;
	
	@Column(name="Competence")
	private String competence;

	@Column(name="Email")
	private String email;

	@Column(name="Experience")
	private String experience;

	@Column(name="Introduction")
	private String introduction;

	@Column(name="Nom")
	private String nom;

	@Column(name="Password")
	private String password;

	@Column(name="Prenom")
	private String prenom;

	@Column(name="Type")
	private String type;

	//bi-directional many-to-one association to Candidature
	@OneToMany(mappedBy="candidat")
	private List<Candidature> candidatures;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="candidat")
	private List<Message> messages;

	//bi-directional many-to-one association to NbrDislike
	@OneToMany(mappedBy="candidat")
	private List<NbrDislike> nbrDislikes;

	//bi-directional many-to-one association to NbrLike
	@OneToMany(mappedBy="candidat")
	private List<NbrLike> nbrLikes;

	//bi-directional many-to-one association to NotificationC
	@OneToMany(mappedBy="candidat")
	private List<NotificationC> notificationCs;

	//bi-directional many-to-one association to Role
	@OneToMany(mappedBy="candidat")
	private List<Role> roles;

	

	public int getCandidatId() {
		return this.candidatId;
	}

	public void setCandidatId(int candidatId) {
		this.candidatId = candidatId;
	}

	

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence(String competence) {
		this.competence = competence;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Candidat() {
		super();
	}
	

	public Candidat(String certification, String competence, String email, String experience, String introduction,
			String nom, String password, String prenom, String type, Integer candidatId) {
		super();
		this.certification = certification;
		this.competence = competence;
		this.email = email;
		this.experience = experience;
		this.introduction = introduction;
		this.nom = nom;
		this.password = password;
		this.prenom = prenom;
		this.type = type;
		this.candidatId=candidatId;
	}
	public Candidat(String certification, String competence, String email, String experience, String introduction,
			String nom, String password, String prenom, String type) {
		super();
		this.certification = certification;
		this.competence = competence;
		this.email = email;
		this.experience = experience;
		this.introduction = introduction;
		this.nom = nom;
		this.password = password;
		this.prenom = prenom;
		this.type = type;
	}
	
	

	

	public List<Candidature> getCandidatures() {
		return this.candidatures;
	}

	public void setCandidatures(List<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public Candidature addCandidature(Candidature candidature) {
		getCandidatures().add(candidature);
		candidature.setCandidat(this);

		return candidature;
	}

	public Candidature removeCandidature(Candidature candidature) {
		getCandidatures().remove(candidature);
		candidature.setCandidat(null);

		return candidature;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setCandidat(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setCandidat(null);

		return message;
	}

	public List<NbrDislike> getNbrDislikes() {
		return this.nbrDislikes;
	}

	public void setNbrDislikes(List<NbrDislike> nbrDislikes) {
		this.nbrDislikes = nbrDislikes;
	}

	public NbrDislike addNbrDislike(NbrDislike nbrDislike) {
		getNbrDislikes().add(nbrDislike);
		nbrDislike.setCandidat(this);

		return nbrDislike;
	}

	public NbrDislike removeNbrDislike(NbrDislike nbrDislike) {
		getNbrDislikes().remove(nbrDislike);
		nbrDislike.setCandidat(null);

		return nbrDislike;
	}

	public List<NbrLike> getNbrLikes() {
		return this.nbrLikes;
	}

	public void setNbrLikes(List<NbrLike> nbrLikes) {
		this.nbrLikes = nbrLikes;
	}

	public NbrLike addNbrLike(NbrLike nbrLike) {
		getNbrLikes().add(nbrLike);
		nbrLike.setCandidat(this);

		return nbrLike;
	}

	public NbrLike removeNbrLike(NbrLike nbrLike) {
		getNbrLikes().remove(nbrLike);
		nbrLike.setCandidat(null);

		return nbrLike;
	}

	public List<NotificationC> getNotificationCs() {
		return this.notificationCs;
	}

	public void setNotificationCs(List<NotificationC> notificationCs) {
		this.notificationCs = notificationCs;
	}

	public NotificationC addNotificationC(NotificationC notificationC) {
		getNotificationCs().add(notificationC);
		notificationC.setCandidat(this);

		return notificationC;
	}

	public NotificationC removeNotificationC(NotificationC notificationC) {
		getNotificationCs().remove(notificationC);
		notificationC.setCandidat(null);

		return notificationC;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role addRole(Role role) {
		getRoles().add(role);
		role.setCandidat(this);

		return role;
	}

	public Role removeRole(Role role) {
		getRoles().remove(role);
		role.setCandidat(null);

		return role;
	}

}