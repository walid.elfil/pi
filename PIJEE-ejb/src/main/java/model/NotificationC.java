package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the NotificationCs database table.
 * 
 */
@Entity
@Table(name="NotificationCs")
@NamedQuery(name="NotificationC.findAll", query="SELECT n FROM NotificationC n")
public class NotificationC implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NotificationCid")
	private int notificationCid;

	private String image;

	private String sending_Date;

	private String title;

	//bi-directional many-to-one association to Candidat
	@ManyToOne
	@JoinColumn(name="Candidatid")
	private Candidat candidat;

	public NotificationC() {
	}

	public int getNotificationCid() {
		return this.notificationCid;
	}

	public void setNotificationCid(int notificationCid) {
		this.notificationCid = notificationCid;
	}

	

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getSending_Date() {
		return sending_Date;
	}

	public void setSending_Date(String sending_Date) {
		this.sending_Date = sending_Date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

}