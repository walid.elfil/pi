package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Messages database table.
 * 
 */
@Entity
@Table(name="Messages")
@NamedQuery(name="Message.findAll", query="SELECT m FROM Message m")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Messageid")
	private int messageid;

	private String content;

	@Column(name="Entrepriseid")
	private int entrepriseid;

	private String sender;

	//bi-directional many-to-one association to Candidat
	@ManyToOne
	@JoinColumn(name="Candidatid")
	private Candidat candidat;

	public Message() {
	}

	public int getMessageid() {
		return this.messageid;
	}

	public void setMessageid(int messageid) {
		this.messageid = messageid;
	}

	
	public int getEntrepriseid() {
		return this.entrepriseid;
	}

	public void setEntrepriseid(int entrepriseid) {
		this.entrepriseid = entrepriseid;
	}

	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

}