package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Roles database table.
 * 
 */
@Entity
@Table(name="Roles")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Roleid")
	private int roleid;

	@Column(name="Nom")
	private String nom;

	//bi-directional many-to-one association to Candidat
	@ManyToOne
	@JoinColumn(name="Candidatid")
	private Candidat candidat;

	//bi-directional many-to-one association to Entrepris
	@ManyToOne
	@JoinColumn(name="Entrepriseid")
	private Entrepris entrepris;

	public Role() {
	}

	public int getRoleid() {
		return this.roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Entrepris getEntrepris() {
		return this.entrepris;
	}

	public void setEntrepris(Entrepris entrepris) {
		this.entrepris = entrepris;
	}

}