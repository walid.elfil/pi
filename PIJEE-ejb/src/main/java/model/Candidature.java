package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Candidature database table.
 * 
 */
@Entity
@NamedQuery(name="Candidature.findAll", query="SELECT c FROM Candidature c")
public class Candidature implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="Age")
	private int age;

	@Column(name="Etat")
	private String etat;

	@Column(name="First_Name")
	private String first_Name;

	@Column(name="Job")
	private String job;

	@Column(name="Last_Name")
	private String last_Name;

	@Column(name="Sex")
	private String sex;

	//bi-directional many-to-one association to Candidat
	@ManyToOne
	@JoinColumn(name="Candidat_CandidatId")
	private Candidat candidat;

	//bi-directional many-to-one association to Entretien
	@ManyToOne
	@JoinColumn(name="Entretienid")
	private Entretien entretien;

	public Candidature() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getFirst_Name() {
		return first_Name;
	}

	public void setFirst_Name(String first_Name) {
		this.first_Name = first_Name;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getLast_Name() {
		return last_Name;
	}

	public void setLast_Name(String last_Name) {
		this.last_Name = last_Name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Entretien getEntretien() {
		return this.entretien;
	}

	public void setEntretien(Entretien entretien) {
		this.entretien = entretien;
	}

	@Override
	public String toString() {
		return "Candidature [id=" + id + ", age=" + age + ", etat=" + etat + ", first_Name=" + first_Name + ", job="
				+ job + ", last_Name=" + last_Name + ", sex=" + sex + ", candidat=" + candidat + ", entretien="
				+ entretien + "]";
	}

	public Candidature(int age, String etat, String first_Name, String job, String last_Name, String sex) {
		super();
		this.age = age;
		this.etat = etat;
		this.first_Name = first_Name;
		this.job = job;
		this.last_Name = last_Name;
		this.sex = sex;
	}
	
	

}