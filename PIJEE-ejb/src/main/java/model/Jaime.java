package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Jaimes database table.
 * 
 */
@Entity
@Table(name="Jaimes")
@NamedQuery(name="Jaime.findAll", query="SELECT j FROM Jaime j")
public class Jaime implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="jaime_id")
	private int jaimeId;

	@Column(name="jaime_count")
	private int jaimeCount;

	@Column(name="jaime_txt")
	private String jaimeTxt;

	public Jaime() {
	}

	public int getJaimeId() {
		return this.jaimeId;
	}

	public void setJaimeId(int jaimeId) {
		this.jaimeId = jaimeId;
	}

	public int getJaimeCount() {
		return this.jaimeCount;
	}

	public void setJaimeCount(int jaimeCount) {
		this.jaimeCount = jaimeCount;
	}

	public String getJaimeTxt() {
		return jaimeTxt;
	}

	public void setJaimeTxt(String jaimeTxt) {
		this.jaimeTxt = jaimeTxt;
	}


}