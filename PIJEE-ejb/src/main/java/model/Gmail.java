package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the gmails database table.
 * 
 */
@Entity
@Table(name="gmails")
@NamedQuery(name="Gmail.findAll", query="SELECT g FROM Gmail g")
public class Gmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="To")
	private String to;

	@Column(name="Body")
	private String body;

	@Column(name="From")
	private String from;

	@Column(name="Subject")
	private String subject;

	public Gmail() {
	}

	public String getTo() {
		return this.to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	
}