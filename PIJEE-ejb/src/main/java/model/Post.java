package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Posts database table.
 * 
 */
@Entity
@Table(name="Posts")
@NamedQuery(name="Post.findAll", query="SELECT p FROM Post p")
public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PostId")
	private int postId;

	private String content;

	private Date creationDate;

	private String hashtag;

	@Column(name="nbr_dislike")
	private int nbrDislike;

	@Column(name="nbr_likes")
	private int nbrLikes;

	@Column(name="nom_candidat")
	private String nomCandidat;

	private String type;

	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="post")
	private List<Comment> comments;

	//bi-directional many-to-one association to NbrDislike
	@OneToMany(mappedBy="post")
	private List<NbrDislike> nbrDislikes;

	//bi-directional many-to-one association to NbrLike
	@OneToMany(mappedBy="post")
	private List<NbrLike> nbrLikesSet;

	public Post() {
	}

	public int getPostId() {
		return this.postId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	

	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public String getNomCandidat() {
		return nomCandidat;
	}

	public void setNomCandidat(String nomCandidat) {
		this.nomCandidat = nomCandidat;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getNbrDislike() {
		return this.nbrDislike;
	}

	public void setNbrDislike(int nbrDislike) {
		this.nbrDislike = nbrDislike;
	}

	public int getNbrLikes() {
		return this.nbrLikes;
	}

	public void setNbrLikes(int nbrLikes) {
		this.nbrLikes = nbrLikes;
	}

	

	

	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setPost(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setPost(null);

		return comment;
	}

	public List<NbrDislike> getNbrDislikes() {
		return this.nbrDislikes;
	}

	public void setNbrDislikes(List<NbrDislike> nbrDislikes) {
		this.nbrDislikes = nbrDislikes;
	}

	public NbrDislike addNbrDislike(NbrDislike nbrDislike) {
		getNbrDislikes().add(nbrDislike);
		nbrDislike.setPost(this);

		return nbrDislike;
	}

	public NbrDislike removeNbrDislike(NbrDislike nbrDislike) {
		getNbrDislikes().remove(nbrDislike);
		nbrDislike.setPost(null);

		return nbrDislike;
	}

	public List<NbrLike> getNbrLikesSet() {
		return this.nbrLikesSet;
	}

	public void setNbrLikesSet(List<NbrLike> nbrLikesSet) {
		this.nbrLikesSet = nbrLikesSet;
	}

	public NbrLike addNbrLikesSet(NbrLike nbrLikesSet) {
		getNbrLikesSet().add(nbrLikesSet);
		nbrLikesSet.setPost(this);

		return nbrLikesSet;
	}

	public NbrLike removeNbrLikesSet(NbrLike nbrLikesSet) {
		getNbrLikesSet().remove(nbrLikesSet);
		nbrLikesSet.setPost(null);

		return nbrLikesSet;
	}

}