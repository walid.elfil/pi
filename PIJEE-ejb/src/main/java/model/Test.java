package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Tests database table.
 * 
 */
@Entity
@Table(name="Tests")
@NamedQuery(name="Test.findAll", query="SELECT t FROM Test t")
public class Test implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Testid")
	private int testid;

	public Test() {
	}

	public int getTestid() {
		return this.testid;
	}

	public void setTestid(int testid) {
		this.testid = testid;
	}

}