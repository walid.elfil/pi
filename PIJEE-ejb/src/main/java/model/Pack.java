package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Packs database table.
 * 
 */
@Entity
@Table(name="Packs")
@NamedQuery(name="Pack.findAll", query="SELECT p FROM Pack p")
public class Pack implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Packid")
	private int packid;

	private int nbrPoint;

	private String nomPack;

	public Pack() {
	}

	public int getPackid() {
		return this.packid;
	}

	public void setPackid(int packid) {
		this.packid = packid;
	}

	public int getNbrPoint() {
		return this.nbrPoint;
	}

	public void setNbrPoint(int nbrPoint) {
		this.nbrPoint = nbrPoint;
	}

	public String getNomPack() {
		return nomPack;
	}

	public void setNomPack(String nomPack) {
		this.nomPack = nomPack;
	}

	

}