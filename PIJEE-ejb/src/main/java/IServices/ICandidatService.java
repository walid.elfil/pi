package IServices;

import java.util.List;



import javax.ejb.Remote;

import model.Candidat;
import model.Candidature;

@Remote
public interface ICandidatService {
	public int addCandidat(Candidat Candidat);
	public List<Candidat> getAllCandidat();
	public void removeCandidatById(int candidatId);
	public void updateCandidat(Candidat Candidat);
	 Candidat getEmployeByEmailAndPassword(String email, String password);
	public int nbrCandidatparComp(String c);


}
