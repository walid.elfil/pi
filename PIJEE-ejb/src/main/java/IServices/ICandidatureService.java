package IServices;

import java.util.List;


import javax.ejb.Remote;

import model.Candidature;

@Remote
public interface ICandidatureService {
	public int addCandidature(Candidature Candidature);
	public List<Candidature> getAllCandidatures();

}
