package Services;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import IServices.ICandidatureService;
import model.Candidature;

@Stateless
@LocalBean
public class CandidatureService implements ICandidatureService {
	@PersistenceContext
	EntityManager em;

	@Override
	public int addCandidature(Candidature Candidature) {
		em.persist(Candidature);
		return Candidature.getId();
	}

	@Override
	public List<Candidature> getAllCandidatures() {
		List<Candidature> emp = em.createQuery("Select t from Candidature t", Candidature.class).getResultList();
		return emp;
	}

	
}
