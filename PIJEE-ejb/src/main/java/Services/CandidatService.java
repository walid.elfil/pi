package Services;





import java.util.List;

import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import IServices.ICandidatService;
import model.Candidat;


@Stateless
@LocalBean
public class CandidatService implements ICandidatService {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public int addCandidat(Candidat Candidat) {
		em.persist(Candidat);
		return Candidat.getCandidatId();
	}

	@Override
	public List<Candidat> getAllCandidat() {
		List<Candidat> emp = em.createQuery("Select c from Candidat c ", Candidat.class).getResultList();
		return emp;

	}

	@Override
	public void removeCandidatById(int candidatId) {
		em.remove(em.find(Candidat.class, candidatId));
		
	}

	@Override
	public void updateCandidat(Candidat Candidat) {
		em.merge(Candidat);

		
		

		
	}

	@Override
	public Candidat getEmployeByEmailAndPassword(String email, String password) {
		TypedQuery<Candidat> query = em.createQuery("Select e from Candidat e "
				+ "where e.email=:email and " 
						+ "e.password=:password",Candidat.class) ; 
				query.setParameter("email", email);
				query.setParameter("password", password); 
				Candidat candidat = null; 
				try{
					candidat = query.getSingleResult(); 
					}catch(NoResultException e){ 
						Logger.getAnonymousLogger().info("Aucun employe trouve avec email : " + email);
				//Logger.getAnonymousLogger.info s'iI n'est pas déjà defini
				}
				return candidat; 

	}

	public int nbrCandidatparComp(String c) {
		int x;
		System.out.println(c+"mta service ");
				
				 Query query = this.em.createQuery("SELECT competence , count(*) FROM Candidat r  WHERE r.competence="+c+" GROUP BY r.competence ");
				    //query.setParameter("comp", c);
				int i =0 ;
				System.out.println("this i1 f service iiiiiiiiiiiiiiiiiiiiii "+i);
				
					i = (int) query.getSingleResult(); 
			
				System.out.println("this i f service iiiiiiiiiiiiiiiiiiiiii "+i);
				return i; 

}
				 
		
	}


